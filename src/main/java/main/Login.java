package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Login {

    public Login (WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void loginMethod(WebDriver driver, String userName, String password)  {
           driver.navigate().refresh();
        driver.findElement(By.name("username")).sendKeys(userName);
        driver.findElement(By.name("password")).sendKeys(password);
      //  Thread.sleep(5000);
     //   driver.findElement(By.className("btn btn-action btn-lg btn-block loginbtn")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
    }

}

package test;

import main.Login;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class LoginTest {

    public static WebDriver driver;

     @BeforeClass
     public void setup(){
        System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Selenium\\chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.phptravels.net/login");

   }
@Test
    public void validLogin() {
    Login log = new Login(driver);
     log.loginMethod(driver, "user@phptravels.com", "demouser");

}
   @AfterClass
    public void quit(){
     driver.close();
   }

}
